import java.util.Scanner;

public class Soma {
	public static void main(String[] args) {
			Scanner sc = new Scanner(System.in);

		    // numero1, numero2, numero3, numero4, soma são variáveis que irão estar na memória(RAM) do computador
		    // para você informar que a variável é inteiro para o C você usa a palavra reservada "int"
		    // Na linha abaixo declaramos variáveis
		    int numero1, numero2, numero3, numero4, soma; // no final da linha tem ponto e vírgula
		    
		    // preciso realizar a leitura de quatro número
		    
		    // Para leitura/entrada de dados vamos usar scanf 
		    // para scanf ler valor inteiro ele precisa do "%d"
		    // Se fosse real(float ) seria %f
		    System.out.println("Entre com o primeiro numero ");
		    numero1 = sc.nextInt();  // leia numero1

		    System.out.printf("Entre com o segundo numero ");
		    numero2 = sc.nextInt(); // leia numero2

		    System.out.printf("Entre com o terceiro numero ");
		    numero3=sc.nextInt();; // leia numero3

		    System.out.printf("Entre com o quarto numero ");
		    numero4 = sc.nextInt(); // leia numero4
		    
		    // cálculo da soma
		    // sinal =(igual) é para atribuição 
		    soma = numero1 + numero2 + numero3 + numero4;
		    
		    
		    // para mostrar na tela vamos usar printf
		    // para dizer para o printf que você quer exibir variável inteira use "%d"
		    System.out.printf("A soma dos quatros numeros eh %d", soma); //escreva soma
	}
}
