/*
Questao 2)	Faça um algoritmo e programa em C que receba três notas, calcule e mostre a média aritmética entre elas.
Entender o problema 
O que deseja o enunciado? 
Ele quer um algoritmo e também quer um programa em C

Quais são as entrada de dados ?
receba três notas

Quais os valores das entradas de dados ?
São reais ou inteiros ?
Eu sei que notas podem valores com casas decimais. Por exemplo 9.5, 5, 5.1

Quais são os processamentos ?
calcule a média aritmética entre elas

Como é que faz o cálculo de uma média artimética considerando que eu tenho 3 notas(nota1, nota2, nota3)

media_aritmetica <- (nota1 + nota2 + nota3) / 3 

Quais são as saídas de dados ?
mostre a média aritmética entre elas

Algoritmo
    real     nota1, nota2, nota3
    
    leia nota1 
    leia nota2
    leia nota3
    
    media_aritmetica <- (nota1 + nota2 + nota3) / 3 

    escreva media_aritmetica
FimAlgoritmo

*/

#include <stdio.h>

int main( ) {

    // meu programa fica aqui    
    float  nota1, nota2, nota3, media_aritmetica;

    printf("Entre com nota1 "); // apenas informativo para o usuario    
    scanf("%f", &nota1); // leia nota1
    
    printf("Entre com nota2 ");
    scanf("%f", &nota2); //leia nota2
    
    printf("Entre com nota3 ");
    scanf("%f", &nota3); // leia nota3
    
    media_aritmetica = (nota1 + nota2 + nota3) / 3; 

    printf("A media aritmetica e %f", media_aritmetica); //escreva media_aritmetica

    
    return 1;
}



