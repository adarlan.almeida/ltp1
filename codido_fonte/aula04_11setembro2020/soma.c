/*
Questão 1
# Descrição do problema
# Entender o problema
1.	Faça/Construa um algoritmo 
2. Criar um programa em C 

Quais são as entradas para o algoritmo ?
3. que receba(entrada de dados) quatro números inteiros, - Entrada de dados quatro 
# números(quatro variáveis) inteiros(tipo de variável inteira) 

O que o algoritmo precisa processar ?
calcule a soma desses números.

O que o algoritmo precisa mostrar ou retornar para o usuário ?
# mostre(ele quer uma operação de escreva ou seja que apareça na tela) a soma desses números.

Portugol

Algoritmo
    inteiro numero1, numero2, numero3, numero4, soma
    
    leia numero1
    leia numero2
    leia numero3
    leia numero4
    
    // soma está recebendo 
    soma <- numero1 + numero2 + numero3 + numero4
    
    escreva soma
    
FimAlgoritmo
*/


// estou usando uma biblioteca de entrada e saída
#include   <stdio.h>  // esta linha de código é para entrar com dados  e mostrar dados na tela

//vou começar a criar meu program

int main () {

    // numero1, numero2, numero3, numero4, soma são variáveis que irão estar na memória(RAM) do computador
    // para você informar que a variável é inteiro para o C você usa a palavra reservada "int"
    // Na linha abaixo declaramos variáveis
    int numero1, numero2, numero3, numero4, soma; // no final da linha tem ponto e vírgula
    
    // preciso realizar a leitura de quatro número
    
    // Para leitura/entrada de dados vamos usar scanf 
    // para scanf ler valor inteiro ele precisa do "%d"
    // Se fosse real(float ) seria %f
    printf("Entre com o primeiro numero ");
    scanf("%d", &numero1);  // leia numero1

    printf("Entre com o segundo numero ");
    scanf("%d", &numero2); // leia numero2

    printf("Entre com o terceiro numero ");
    scanf("%d", &numero3); // leia numero3

    printf("Entre com o quarto numero ");
    scanf("%d", &numero4); // leia numero4
    
    // cálculo da soma
    // sinal =(igual) é para atribuição 
    soma = numero1 + numero2 + numero3 + numero4;
    
    
    // para mostrar na tela vamos usar printf
    // para dizer para o printf que você quer exibir variável inteira use "%d"
    printf("A soma dos quatros numeros eh %d", soma); //escreva soma
    
    return 1;    
}

