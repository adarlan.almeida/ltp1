#include <stdio.h>

int main() {
	int idade=3200099999999999999;  // digamos que int seja 16 bits como sei quantos numeros representar ?
	                          // 2 ^ 16 = 65536.  negativa 0 positiva  
  
	char letra = 'a';

    // String 
    char *nome = "ricardo";

	// reais
	float salario=9999999999999;
	double distancia;  // precis�o maior que float
	float distancia_menor_precisao = 2.5;
	
	distancia_menor_precisao = 0.00000005 * 0.00000005;
	
	printf("\n%d", idade);
	printf("\n%s", nome);
	printf("\n%f", salario);
	printf("\n%c",letra);
	printf("\n%.20f", distancia_menor_precisao);	
	return 1;
}
