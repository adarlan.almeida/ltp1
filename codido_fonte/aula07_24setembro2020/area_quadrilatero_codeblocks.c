#include <stdio.h>

int     main () {
	// float usarei %f tanto no scanf quanto no printf
	float base, altura, area;

	printf("Entre com a BASE ");
	scanf("%f", &base); // &base ele passa o endere�o de mem�ria da vari�vel base. Isso � um ponteiro

	printf("Entre com a ALTURA ");
	scanf("%f", &altura);

	area = base * altura;

	printf("A area � %.2f ", area);

	return 1;
}
