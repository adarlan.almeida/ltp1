#include  <stdio.h> // incluindo uma biblioteca no programa. Somente assim consigo usar printf e scanf

/*
Aluno: Ricardo
Objetivo: Calcula media e informar se aluno esta aprovado ou reprovado
Data: 24/09/2020

*/

int main() {
	float nota1=0.0, nota2=0.0, media=0.0;
	
	// entrada de dados para nota1
	printf("Entre com a nota 1 ");
	scanf("%f", &nota1);

    // entrada de dados para nota2
	printf("Entre com a nota 2 ");
	scanf("%f", &nota2);
	
	// calcular a media aritmetica de nota1 e nota2
    media = (nota1 + nota2) / 2;
	
	printf("\nA media do aluno foi %f ", media);

	if ( media >= 5.0 ) {   // se media > 5 entao
		printf("\nAluno aprovado");	  //    escreval("Aluno Aprovado")      // comando1
    } else {   // sen�o
		printf("\nAluno reprovado");	
	}
	
	return 1;
}
