/*
operadores aritméticos
+ soma
/ divisão
* multiplicação
- subtração

operadores relacionais
Se o resultado da expressão relacional for verdade eh igual a 1
Se o resultado da expressão relacional for falso eh igual a 0
>
>=
<
<=

operadores lógicos
E  &&
OU ||
NEGAÇÃO !
*/
#include <stdio.h>

int main() {
    int numero1, numero2, expressao;
    numero1 = 5;
    numero2 = 9;
    expressao = numero1 < numero2;
    printf ("Expressao %d < %d eh igual a %d ", numero1 , numero2, expressao);
    
    numero1 = 15;
    numero2 = 9;
    expressao = numero1 < numero2;
    printf ("\nExpressao %d < %d eh igual a %d ", numero1 , numero2, expressao);
    return 1;   
}