/*
   Verdadeiro ser� 1, mas na verdade todo valor diferente de zero � Verdadeiro
   Falso ser� 0

   Nega��o    !

   ==============================================
   a                !a
   Verdadeiro(1)    Falso(0)
   Falso(1)         Verdadeiro(1)
*/

#include <stdio.h> // biblioteca de entrada e sa�da

int main() {
    int a, expressao;

    printf("TABELA DA VERDADE");
    printf("\n======================================================");
    printf("\na\t\t\t !a");
    printf("\n======================================================");
    a = 1;
    expressao = !a;
    printf("\n%d\t\t\t%d", a, expressao);

    a = 0;
    expressao = !a;
    printf("\n%d\t\t\t%d", a, expressao);


    return 1;
}
