	.file	"imprimir_numeros.c"
	.def	___main;	.scl	2;	.type	32;	.endef
	.text
.globl _main
	.def	_main;	.scl	2;	.type	32;	.endef
_main:
	pushl	%ebp
	movl	%esp, %ebp
	andl	$-16, %esp
	subl	$48, %esp
	call	___main
	movl	$0, 44(%esp)
	jmp	L2
L3:
	movl	44(%esp), %eax
	movl	44(%esp), %edx
	movl	%edx, 4(%esp,%eax,4)
	incl	44(%esp)
L2:
	cmpl	$9, 44(%esp)
	jle	L3
	leave
	ret
