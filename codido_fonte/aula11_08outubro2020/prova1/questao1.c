/*
1)	Faça um programa que monte os oito primeiros termos da sequência de
Fibonacci.  0 1 1 2 3 5 8 13 21 34 55 …
            0            1            1           2         3 5 8 13 21 34 55 …
            anterior2    anterior1    proximo
                         anterior2    anterior1    proximo
proximo=anterior1 + anterior2

2=1+1

*/
#include <stdio.h>

int main() {
    int i, proximo, anterior1=1, anterior2=0;
    
    for(i=1; i<=8; i++) {
        
        if(i>=3) {
            proximo=anterior1 + anterior2;
            anterior2=anterior1;
            anterior1=proximo;
            
            printf("%d ", proximo);
        } else if(i==2) {
            printf("%d ", anterior1);
        } else 
            printf("%d ", anterior2);
    }

   return 1;    
}