#include <stdio.h>
/*
Sabe-se que o quilowatt de energia custa um quinto do sal�rio m�nimo. Fa�a
um algoritmo e programa em C que receba o valor do sal�rio m�nimo e a
quantidade de quilowatts consumida por uma resid�ncia. Calcule e mostre:
a) o valor, em reais, de cada quilowatt; 
*/
int main() {
	double salarioMinimo, quantidadeQuilowatts, valorQuilowatt, valor, valorDesconto;
	
	printf("Entre com o salario minimo ");
	scanf("%lf", &salarioMinimo);
	printf("Entre com a quantidade de quilowatts consumida ");
	scanf("%lf", &quantidadeQuilowatts);
	
	valorQuilowatt = salarioMinimo/5.0;
	
	valor = valorQuilowatt*quantidadeQuilowatts;
	
	valorDesconto = valor - valor*0.15; 
	
	printf("\na) o valor, em reais, de cada quilowatt R$ %.2lf", valorQuilowatt);	
	printf("\nb) o valor, em reais, a ser pago por essa resid�ncia R$ %.2lf", valor);	
	printf("\nc) o valor, em reais, a ser pago com desconto de 15 por cento R$ %.2lf", valorDesconto);	
	
	return 1;
	
}
