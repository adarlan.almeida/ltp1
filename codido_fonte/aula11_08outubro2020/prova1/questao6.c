/*
aluno: Ricardo
Prova1 de LTP1
3)	Fa�a um programa que leia um valor N qualquer, inteiro e positivo, calcule e mostre a seguinte soma:
E = 1/1! + 1/2! + 1/3! + ... + 1/N!

1/1!=1/1.00=1.00  + 1/2!=1/2.00=0.50  + 1/3!=1/6.00=0.17  + 1/4!=1/24.00=0.04  + 1/5!=1/120.00=0.01  +
A soma eh 1.72

*/

int main( ) {
	int n, i, j;
	float soma=0.0, fatorial;
	
	printf("Entre com o valor de n ");
	scanf("%d", &n);
	
	for(i=1; i<=n; i++) {
		
		fatorial=1.0;
		
		for(j=1; j<=i; j++) {
			fatorial = fatorial * j;
		}
				
		printf("1/%d!=1/%.2f=%.2f  + ", i, fatorial, 1.0/fatorial);
		
		soma = soma + 1.0/fatorial;
	}
	
	printf("\nA soma eh %.2f", soma);
	
	return 1;
}
