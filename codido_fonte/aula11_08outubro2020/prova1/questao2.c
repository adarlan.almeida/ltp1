#include <stdio.h>

//  1 p� = 12 polegadas, 1 jarda = 3 p�s, 1 milha = 1.760 jardas. 
int main() {
	double polegada, jarda, milha, pe;
	printf("Entre com medida em pes ");
	scanf("%lf", &pe);
	polegada = pe*12.0;
	jarda = 1.0/3.0 * pe;
	milha = 1.0/1760.0 * jarda;
	
	printf("\npe %lf", pe);
	printf("\npolegada %lf", polegada);
	printf("\njarda %lf", jarda);
	printf("\nmilha %lf", milha);
	
	return 1;
}
