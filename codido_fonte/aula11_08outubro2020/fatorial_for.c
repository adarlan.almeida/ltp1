#include <stdio.h>
/*
Aluno: ricardo
Prova1 de LTP1

Quest�o
fatorial de alguns n�meros
1! = 1	= 1	
2! = 2*1 = 2
3! = 3*2*1 = 6
4! = 4*3*2*1 = 24
5! = 5*4*3*2*1 = 1*2*3*4*5 = 2*3*4*5 = 6*4*5 = 24*5 = 120
n! = (n)*(n-1)*(n-2)...1

Crie um programa em C para resolver o fatorial de um determinado n�mero N
Leia o n�mero n

n  	contador 	fatorial
5	1			1*1=1
	2			1*2=2
	3			2*3=6
	4			6*4=24
	5			24*5=120
	6						sai do la�o
	
	O fatorial de n:5 eh 120
*/

int main() {
	int n, 
		fatorial=1, // qualquer valor multiplicado por 1 sera o valor
		contador; // contador come�a por 1 pois quero que ele v� de 1 at� n
	
	printf("Entre com o valor de n ");
	scanf("%d", &n);
	
	//for(inicializa variavel; condicao de parada; incremento/decremento)
	for(contador=1; contador<=n ; contador++) { // contador++ � a mesma coisa que contador=contador+1;
		fatorial = fatorial * contador;
	}
	
	printf("O fatorial de n:%d eh %d", n, fatorial);
	
	return 1;
}

