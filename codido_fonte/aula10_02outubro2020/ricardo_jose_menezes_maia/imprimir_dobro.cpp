#include <stdio.h>

// Crie um programa em C que imprima 10 n�meros
// Cada n�mero da s�rie � composto pelo dobro do n�mero anterior. A s�rie come�a com 1
// 1	1*2=2	2*2=4	4*2=8	8*2=16	
int main() {
	int cont, numero;
	
	cont = 1;
	numero = 1;
	
	while (cont<=10) {
		printf("%d ", numero);

		numero = numero * 2;
		cont = cont + 1;	
	}
	
	return 1;
}
