#include <stdio.h>

// Escreva os 10 primeiros numeros da serie abaixo
// 1 2 3 4 5 6 7 8 9 10

int main() {
	int numero;
	
	numero = 1;
	
	while(numero <= 10) {
		printf("%d ", numero);
		numero = numero + 1;		
	}
	
	return 1;
}
