# Crie um programa em C que imprima 10 números
# Cada número da série é composto pelo dobro do número anterior. A série começa com 1
# 1	1*2=2	2*2=4	4*2=8	8*2=16	

cont = 1
numero = 1
	
while (cont<=10):
    print(" ", numero)
    numero = numero * 2
    cont = cont + 1
