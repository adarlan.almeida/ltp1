#include <stdio.h>

// Crie um programa em C que imprima 8 n�meros
// Cada n�mero da s�rie � composto pelo triplo do n�mero anterior. A s�rie come�a com 1
// 1	1*3=3	3*3=9	9*3=27	27*3=81	
int main() {
	int cont, numero;
	
	cont = 1;
	numero = 1;
	
	while (cont<=8) {
		printf("%d ", numero);

		numero = numero * 3;
		cont = cont + 1;	
	}
	
	return 1;
}
