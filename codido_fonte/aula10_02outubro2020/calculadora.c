#include <stdio.h>

int main() {
	float a, b, resultado;
	char operacao='+';
	
	while(operacao=='+' || operacao=='-' || operacao=='*' || operacao=='/' )  {
		printf("\nEntre com valor de A " );
		scanf("%f", &a);
		
		printf("Entre com valor de B " );
		scanf("%f", &b);
		
		printf("Entre com a opera��o desejada (+ - * /) ");
		scanf("\n%c", &operacao); // limpar o buffer coloco \n e fica portanto a string de controle "\n%c"
		
		if(operacao == '+') 
			resultado = a + b;
		else if(operacao == '-') 
			resultado = a - b;
		else if(operacao == '*') 
			resultado = a * b;
		else if(operacao == '/') 
			resultado = a / b;
		else {
			printf("operacao invalida");
		}
		
		if(operacao=='+' || operacao=='-' || operacao=='*' || operacao=='/' ) 
			printf("\nA:%f %c B:%f = %f", a, operacao, b, resultado );
	}
	
	return 1;
}
