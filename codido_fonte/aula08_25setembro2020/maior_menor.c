#include <stdio.h> // biblioteca padr�o de entrada e sa�da de dados

int    main ( ) {
	// case sensitive
	float a, b, maior, menor;
	
	printf("Entre com A ");
	scanf("%f", &a);

	printf("Entre com B ");
	scanf("%f", &b);
	
	if ( a>b ) {
		maior=a;
		menor=b;
	} else {
		maior=b;
		menor=a;
	}
	
	printf("A:%.2f b:%.3f maior:%.2f menor:%.2f", a, b, maior, menor); 
	
	return 1;
}
