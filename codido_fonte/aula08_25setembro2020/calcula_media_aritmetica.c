#include <stdio.h>

int main() { 
	float nota1, nota2, media;
	int expressao_logica;
	
	printf("Entre com a nota1 ");
	scanf("%f", &nota1);

    printf("Entre com a nota2 ");
    scanf("%f", &nota2);
    
    media = (nota1+nota2) / 2.0;
        
	printf("\nnota1:%.2f nota2:%.2f media:%.2f", nota1, nota2, media);
	
	expressao_logica = media >= 5;
	printf("\nexpressao_logica = media >= 5   : %d", expressao_logica );
	
	// Verdade � 1    tudo que � diferente de 0 � verdadeiro
	// Falso � 0
	if (media >= 5) { 
		printf("\nSituacao: Aluno aprovado"); 
	} else {
		printf("\nSituacao: Aluno reprovado");  //comando2
	}
	
}
