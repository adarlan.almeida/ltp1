#include <stdio.h>

/*
   Crie um programa em C que leia dois n�meros inteiros e imprima qual dos numeros � o maior e qual � o menor
   numero1   numero2   maior  menor
   2		 3         3	  2
   15		 4		   15	  4
*/
int main() {
    int numero1, numero2, maior, menor;
    
    printf("Entre com o primeiro numero ");
    scanf("%d", &numero1);
    
    printf("Entre com o segundo numero ");
    scanf("%d", &numero2);

	if(numero1>numero2) {
	    maior = numero1;
	    menor = numero2;
	} else {
		maior = numero2;
	    menor = numero1;
	}
	
	printf("\nnumero1:%d numero2:%d maior:%d menor:%d", numero1, numero2, maior, menor);
    
	return 1;
}
