#include <stdio.h>

int main( ) {
	int a, b, c;
	printf("Entre com o lado A ");
	scanf("%d", &a);
	printf("Entre com o lado B ");
	scanf("%d", &b);
	printf("Entre com o lado C ");		
	scanf("%d", &c);
	
	if(a<(b+c) && b<(a+c) && c<(a+b)) {
		printf("As medidas: a:%d b:%d c:%d formam um tri�ngulo ", a, b, c);
		
		if(a==b && a==c) 
			printf("EQUILATERO");
		else {
			if ( (a==b) || (a==c) || (b==c) ){
				printf("ISOSCELES");
			} else 
				printf("ESCALENO");	
		}
	} else {
		printf("As medidas: a:%d b:%d c:%d NAO formam um tri�ngulo ", a, b, c);
	}
		
	return 1;
}
