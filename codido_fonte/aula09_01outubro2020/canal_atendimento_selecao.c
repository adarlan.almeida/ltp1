#include <stdio.h>

/*
Voc� foi escolhido para criar um programa para exibir uma lista de op��es para o usu�rio e conforme escolha determinada
op��o voc� ir� imprimir na tela a op��o que ele escolheu.
Informa Caro usuario voc� escolheu a op��o 1 que significa ..........

Como posso ajudar ?

1-Fazer login
2-Esqueci meu login e senha
3-N�o tenho login e senha
4-Pegar o c�digo de barras
5-Ir para o Menu Residencial
6-Sair
 
*/
int main() {
	int opcao;
	printf("Como posso ajudar ?");

	printf("\n1-Fazer login");
	printf("\n2-Esqueci meu login e senha");
	printf("\n3-N�o tenho login e senha");
	printf("\n4-Pegar o c�digo de barras");
	printf("\n5-Ir para o Menu Residencial");
	printf("\n6-Sair\n");
	
	scanf("%d", &opcao);
	
	switch(opcao) {
		case 1: 
			printf("Caro usuario voc� escolheu a op��o 1 que significa Fazer login ");
			break;
		case 2:
			printf("Caro usuario voc� escolheu a op��o 2 que significa Esqueci meu login e senha ");
			break;
		case 3: 	
			printf("Caro usuario voc� escolheu a op��o 3 que significa N�o tenho login e senha ");
			break;
		case 4:
			printf("Caro usuario voc� escolheu a op��o 4 que significa Pegar o c�digo de barras ");
			break;
		case 5:
			printf("Caro usuario voc� escolheu a op��o 5 que significa Ir para o Menu Residencial ");
			break;
		case 6:
			printf("Caro usuario voc� escolheu a op��o 6 que significa Sair ");		
			break;
	}

	return 1;
}
