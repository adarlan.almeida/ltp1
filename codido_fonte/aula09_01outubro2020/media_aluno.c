/*
Considere o problema que calcula a m�dia do aluno e imprime se ele est� aprovado ou reprovado. 
Vamos determinar algumas palavras complementares ao desempenho do aluno de acordo com a m�dia que o aprovou. 
Assim, se a media for maior ou igual a 5 (cinco), al�m de imprimir a palavra APROVADO, vamos imprimir: 
se a m�dia for 10, vamos imprimir �com LOUVOR�;
 se a m�dia for menor do que 10 e maior ou igual a 8, vamos imprimir �com M�RITO�;
e, se for menor do que 8 e maior ou igual a 6, vamos imprimir �com DESTAQUE�.

*/
#include <stdio.h>

int main() {
	float nota1, nota2, nota3, media_aritmetica;
	
	printf("Entre com nota1 ");
	scanf("%f", &nota1);

	printf("Entre com nota2 ");
	scanf("%f", &nota2);

	printf("Entre com nota3 ");
	scanf("%f", &nota3);
	
	media_aritmetica = (nota1 + nota2 + nota3) / 3;
	
	if (media_aritmetica>=5) {
		printf("Aprovado ");
		
		if(media_aritmetica==10) {
			printf("com LOUVOR");
		} else {
			if(media_aritmetica >= 8) {
				printf("com M�RITO");
			} else {
				if (media_aritmetica>=6) {
					printf("com DESTAQUE");
				}
			}
		}
	} else {
		printf("Reprovado ");
	}
	
	return 1;
}

