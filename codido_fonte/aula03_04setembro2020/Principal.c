/*
Algoritmo: Ler dois números e vou exibir a soma dos números para o usuário 
   leia primeiro_numero  // Entrada de dados
   leia segundo_numero   // Entrada de dados
   soma <- primeiro_numero + segundo_numero 
   escreva soma // saida de dados
FimAlgoritmo
*/
#include <stdio.h> // toda vez adicionar esta linha

int main() {
   int primeiro_numero, segundo_numero, soma;
   printf("Entre com o primeiro número ");
   scanf("%d", &primeiro_numero); // entrada de dados
   printf("Entre com o segundo número ");
   scanf("%d", &segundo_numero); // entrada de dados
   soma = primeiro_numero + segundo_numero;
   printf("A soma é %d", soma);  // saida de dados
   return 1;    
}
