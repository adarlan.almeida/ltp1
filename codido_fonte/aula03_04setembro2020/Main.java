/*
Algoritmo: Ler dois números e vou exibir a soma dos números para o usuário 
   leia primeiro_numero  // Entrada de dados
   leia segundo_numero   // Entrada de dados
   soma <- primeiro_numero + segundo_numero 
   escreva soma // saida de dados
FimAlgoritmo
*/
import java.util.Scanner;

public class Main {
    public static void main(String args[]) {
       Scanner leitor = new Scanner(System.in);
       int primeiro_numero, segundo_numero, soma;
       System.out.printf("Entre com o primeiro número ");
       primeiro_numero = leitor.nextInt(); // entrada de dados
       System.out.printf("Entre com o segundo número ");
       segundo_numero  = leitor.nextInt(); // entrada de dados
       soma = primeiro_numero + segundo_numero;
       System.out.printf("A soma é %d", soma);  // saida de dados
    }
}

