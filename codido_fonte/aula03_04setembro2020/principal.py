'''
Algoritmo: Ler dois números e vou exibir a soma dos números para o usuário 
   leia primeiro_numero  // Entrada de dados
   leia segundo_numero   // Entrada de dados
   soma <- primeiro_numero + segundo_numero 
   escreva soma // saida de dados
FimAlgoritmo
'''
print("Entre com o primeiro número ")
primeiro_numero = int(input()) # entrada de dados
print("Entre com o segundo número ")
segundo_numero = int(input()) # entrada de dados

soma = primeiro_numero + segundo_numero

print("A soma é ", soma) 