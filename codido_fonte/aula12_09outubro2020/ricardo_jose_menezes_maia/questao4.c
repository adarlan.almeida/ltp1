/*
4) Sabe-se que o quilowatt de energia custa um quinto do sal�rio m�nimo. Fa�a
um algoritmo e programa em C que receba o valor do sal�rio m�nimo e a
quantidade de quilowatts consumida por uma resid�ncia. Calcule e mostre:
a) o valor, em reais, de cada quilowatt;
b) o valor, em reais, a ser pago por essa resid�ncia;
c) o valor, em reais, a ser pago com desconto de 15%.

Aluno: Ricardo
Prova1 de LTP1
*/
#include <stdio.h>

int main() {
	float salarioMinimo, quantidadeQuilowatts, quilowatt, valorPagoResidencia, valorDesconto;
	
	printf("Entre com o valor do salario minimo ");
	scanf("%f", &salarioMinimo);

	printf("Entre com a quantidade de quilowatts ");
	scanf("%f", &quantidadeQuilowatts);
	
	quilowatt = salarioMinimo * 1.0/5; // valor de 1 quilowatt
	
	valorPagoResidencia = quilowatt * quantidadeQuilowatts; 
	
	valorDesconto = valorPagoResidencia - valorPagoResidencia*15.0/100;
	
	printf("\na) o valor, em reais, de cada quilowatt: %f", quilowatt);
	printf("\nb) o valor, em reais, a ser pago por essa resid�ncia: %f ", valorPagoResidencia);
	printf("\nc) o valor, em reais, a ser pago com desconto de 15%: %f", valorDesconto);
	
	return 1;
}
