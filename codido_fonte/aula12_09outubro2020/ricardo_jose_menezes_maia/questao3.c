//3) Fa�a um programa que receba tr�s n�meros e mostre-os em ordem crescente.

#include <stdio.h>

int main() {
	int a,b,c; // 3*2*1
	
	printf("Entre com o valor de a ");
	scanf("%d", &a);
	printf("Entre com o valor de b ");
	scanf("%d", &b);
	printf("Entre com o valor de c ");
	scanf("%d", &c);

	//c 	b 	a
	//b 	c 	a	
	if(a>b && a>c) {
		if(b>c)
			printf("%d %d %d", c, b, a);
		else		
			printf("%d %d %d", b, c, a);
	} else if(b>a && b>c) {
		//c 	a 	b
		//a 	c 	b	
		if(a>c)
			printf("%d %d %d", c, a, b);
		else			
			printf("%d %d %d", a, c, b);
	} else {
		//b 	a 	c
		//a 	b 	c	
		if(a>b)
			printf("%d %d %d", b, a, c);
		else 
			printf("%d %d %d", a, b, c);
	}
	
	return 1;
}
