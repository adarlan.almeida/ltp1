/*
1) Fa�a um programa que monte os n primeiros termos da sequ�ncia de
Fibonacci. 0 1 1 2 3 5 8 13 21 34 55 �

leia o valor de n

Aluno: Ricardo
Prova parcial de LTP1
*/
#include <stdio.h>

int main( ) {
	int i, n, anterior2=0, anterior1=1, proximo;
	
	printf("Entre com o valor de n ");
	scanf("%d", &n);
	
	for(i=1; i<=n; i++) {
		if(i==1) 
			proximo=0;
		else if(i==2)
			proximo=1;
		else {
			proximo=anterior1+anterior2;
			anterior2=anterior1;
			anterior1=proximo;
		}

		printf("%d ", proximo);
	}
		
	return 1;
}
