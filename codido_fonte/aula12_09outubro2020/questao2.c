/*
Questao 2) Sabe-se que: 1 p� = 12 polegadas, 1 jarda = 3 p�s, 1 milha = 1.760 jardas.
Fa�a um algoritmo e programa em C que receba uma medida em p�s, fa�a as
convers�es a seguir e mostre os resultado
a) Polegadas
b) Jardas
c) Milhas
Aluno: Ricardo
Prova1 de LTP1
*/

int main() {
	double pes, polegadas, milhas, jardas;
	
	printf("Entre com medida em pes ");
	scanf("%lf", &pes); // %lf � para ler e imprimir double
						// %f quando for float
	
	polegadas=pes*12;  //  1 p� = 12 polegadas
	jardas=pes*(1.0/3);	//1 jarda = 3 p�s => 1 pe = 1/3 jardas
	milhas=1.0/1760 * jardas;  // 1 milhas = 1.760 jardas => 1 jarda = 1/1760 milhas
	
	printf("\npes:%lf polegadas:%lf milhas:%lf jardas:%lf", pes, polegadas, milhas, jardas);
	
	return 1;
}
