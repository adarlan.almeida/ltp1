/*
Um m�todo para o c�lculo de raiz quadradas de um n�mero N j� era conhecido
pelos babil�nios em... bom, h� muito tempo (tamb�m � conhecido como
M�todo de Heron, um matem�tico grego que o descreveu 20 s�culos depois,
perto do ano 50 DC). Come�ando com um valor inicial k (geralmente valendo
1), os babil�nios geravam um novo valor de k de acordo com a regra: k = (k + N/k)/2 
A medida em que o processo � repetido, os novos valores de k se
aproximam cada vez mais da raiz de N. Fa�a um programa que leia o valor de
N e exiba os primeiros doze valores calculados com essa f�rmula, verificando
se eles realmente se aproximaram da raiz correta.
*/
#include <stdio.h>
#include <math.h>

int main() {
	double k=1, n;
	int i;
	
	printf("Entre com o valor de n ");
	scanf("%lf", &n); // %lf porque estamos lendo double
	
	for(i=1; i<=12; i++) {
		k = (k + n/k)/2;
	}
	
	printf("\nRaiz pelo metodo Heron %lf ", k);
	printf("\nRaiz da biblioteca math %lf ", sqrt(n)); // calcula raiz quadrada de k
	
	return 1;
}
