/*
1) Fa�a um programa que monte os oito primeiros termos da sequ�ncia de
Fibonacci. 0 1 1 2 3 5 8 13 21 34 55 �

0 				1 				1 					2 				3 								5 8 13 21 34 55
								anterior2		anterior1			proximo=anterior1+anterior2
Aluno: Ricardo
Prova parcial de LTP1
*/
#include <stdio.h>

int main( ) {
	int i, anterior2=0, anterior1=1, proximo;
	
	for(i=1; i<=10; i++) {
		if(i==1) 
			proximo=0;
		else if(i==2)
			proximo=1;
		else {
			proximo=anterior1+anterior2;
			anterior2=anterior1;
			anterior1=proximo;
		}

		printf("%d ", proximo);
	}
		
	return 1;
}
