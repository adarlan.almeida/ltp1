/*
Operadores artimeticos

+ soma Exemplo   2+5
* multiplicacao  2*5
/ divisao        2/5
- subtração      2-5

Problema Escreva um algoritmo que leia dois numeros e imprima a soma, subtração, multiplicação 
  e divisão destes dois números
Algoritmo: 
    inteiro numero1, numero2, soma, subtracao, multicacao
    real divisao
    
    leia numero1
    leia numero2
    soma = numero1 + numero2
    subtracao = numero1 - numero2
    multicacao = numero1 * numero2
    divisao  = numero1 / numero2
    escreva soma, subtracao, multicacao, divisao
FimAlgoritmo

*/

// para compilar e executar  
// gcc calculadora.c -o calculadora
// calculadora.exe

#include <stdio.h>
int main() {
    int numero1, numero2, soma, subtracao, multicacao;
    float divisao; // pode receber casa decimal   
    
    printf("Entre com o numero1 ");
    scanf("%d", &numero1);  // %d porque numero1 é int
    
    printf("Entre com o numero2 ");
    scanf("%d", &numero2);  // %d porque numero2 é int
    soma = numero1 + numero2;  // resultado será um inteiro int
    subtracao = numero1 - numero2; // resultado será um inteiro int
    multicacao = numero1 * numero2; // resultado será um inteiro int
    
    // ((float)numero2) é um cast que significa que estou transformando um int em float
    divisao  = numero1 / (float)numero2 ; // ((float)numero2) é float logo a expressao irá retornar float
    
    printf("soma:%d, subtracao:%d , multicacao:%d , divisao:%f ", soma, subtracao, multicacao, divisao);
    
    return 1;
}


