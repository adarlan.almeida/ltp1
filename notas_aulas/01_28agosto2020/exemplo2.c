/*

Express�es relacionais

>   maior
<   menor
>=  maior ou igual
<=  menor ou igual
==  igual

o resultado em algoritmo ser� VERDADEIRO OU FALSO

*/

#include <stdio.h>

int main() {
    int numero1=5, numero2=5, expressao_relacional;

    //VERDADEIRO SERA 1 -- SERA TUDO QUE EH DIFERENTE DE ZERO
    // FALSO SERA 0
    expressao_relacional = numero1 > numero2;
    printf("\n%d > %d EH %d", numero1, numero2, expressao_relacional);

    expressao_relacional = numero1 >= numero2;
    printf("\n%d >= %d EH %d", numero1, numero2, expressao_relacional);

    expressao_relacional = numero1 < numero2;
    printf("\n%d < %d EH %d", numero1, numero2, expressao_relacional);

    expressao_relacional = numero1 <= numero2;
    printf("\n%d <= %d EH %d", numero1, numero2, expressao_relacional);

    expressao_relacional = numero1 == numero2;
    printf("\n%d == %d EH %d", numero1, numero2, expressao_relacional);

    return 1;
}


