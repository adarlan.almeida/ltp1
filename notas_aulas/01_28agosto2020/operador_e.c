/*
   Verdadeiro ser� 1, mas na verdade todo valor diferente de zero � Verdadeiro
   Falso ser� 0

   Operador OU   ||
   Operador E    &&

   ==============================================
   a                b               a && b
   Verdadeiro(1)    Verdadeiro(1)   Verdadeiro(1)
   Verdadeiro(1)    Falso(0)        Falso(0)
   Falso(0)         Verdadeiro(1)   Falso(0)
   Falso(0)         Falso(0)        Falso(0)
*/

#include <stdio.h> // biblioteca de entrada e sa�da

int main() {
    int a, b, expressao;

    printf("TABELA DA VERDADE");
    printf("\n======================================================");
    printf("\na\t\t\tb\t\t\ta && b");
    printf("\n======================================================");
    a = 1; b = 1;
    expressao = a && b;
    printf("\n%d\t\t\t%d\t\t\t%d", a, b, expressao);

    a = 1; b = 0;
    expressao = a && b;
    printf("\n%d\t\t\t%d\t\t\t%d", a, b, expressao);

    a = 0; b = 1;
    expressao = a && b;
    printf("\n%d\t\t\t%d\t\t\t%d", a, b, expressao);

    a = 0; b = 0;
    expressao = a && b;
    printf("\n%d\t\t\t%d\t\t\t%d", a, b, expressao);

    return 1;
}
