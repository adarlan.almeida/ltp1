# Bem-vindo ao Curso

### Engenharia da Computação

#### Disciplina: **Linguagem e Técnicas de Programação I**

#### Código da turma: **vpv2yrj**

#### Professor: Professor MSc. Ricardo José Menezes Maia.  http://lattes.cnpq.br/0706885145380777
* Doutorado, Universidade de Brasília UNB - Ciência da Computação - 2019 - em andamento
* Mestrado, Universidade de São Paulo USP - Escola Politécnica, Engenharia de Computação - 2008 a 2010
* Bacharelado em Ciência da Computação, Universidade Federal do Amazonas - 1999 a 2003
* Técnico em Programação, Fundação de Ensino e Pesquisa Matias Machline - Sharp do Brasil - 1992 a 1994.

Desejo que nossa experiência neste curso seja a melhor possível!

Abaixo está o sumário do curso e para baixar todo conteúdo que está versionado no git clique no link
https://gitlab.com/ricardo.jmm/ltp1/-/archive/master/ltp1-master.zip

Após isso descompacte o arquivo em uma pasta da sua escolha


Plano de Ensino está no arquivo Plano_LTP1_prof_RicardoMaia.docx
[Plano de Ensino](Plano_LTP1_prof_RicardoMaia.docx)


* A disciplina terá 3 avaliações:

* * Primeira avaliação data provável: 08/10/2020

* * Segunda avaliação data provável: 06/11/2020

* * Terceira avaliação data provável: 04/12/2020


Link das aulas mediadas pelo google meet:

* 21/08/2020 - https://meet.google.com/okx-twvy-ydh
* * Gravação - https://drive.google.com/file/d/1VY495yzCj8u81RjKCPJuLFnGqenP2PrJ/view?usp=sharing

* 28/08/2020 - https://meet.google.com/umt-mkir-oit
* * Gravação - https://drive.google.com/file/d/1inghin9UdG0fMV8o25Gz3oYBSiXBX9lz/view?usp=sharing

* 04/09/2020 - https://meet.google.com/eat-mfka-mvo
* * Gravação https://drive.google.com/file/d/1V32rW8oZh9Qnv16h5HoRKtzcLLT5c5-2/view?usp=sharing

* 11/09/2020 - https://meet.google.com/eki-tgca-xgm
* * Gravação https://drive.google.com/file/d/1FRgd3xAARlc8uM1TZCdSvF655boeMdxg/view?usp=sharing

* 17/09/2020 - https://meet.google.com/ndc-atjc-jwt
* * Gravação https://drive.google.com/file/d/1yyWz1sCXXoeqN1BwVrvaadzX4Q3wkz0h/view?usp=sharing

* 18/09/2020 - https://meet.google.com/fob-budh-qjo
* * Gravação https://drive.google.com/file/d/1I4UNwV9Z0r7GiLyYxrxiAN4O6-XQ8djj/view?usp=sharing

* 24/09/2020 - https://meet.google.com/iew-hqyq-spk
* * Gravação https://drive.google.com/file/d/1PNAoUeRcOCssbJIIrace0lriZIy18_wW/view?usp=sharing

* 25/09/2020 - https://meet.google.com/jqw-sene-qzr
* * Gravação https://drive.google.com/file/d/1kXkvVjsXMNwQzhv3AEEwLvhKYxNGauiJ/view?usp=sharing

* 01/10/2020 - https://meet.google.com/fgs-nugg-sfy
* * Gravação https://drive.google.com/file/d/1C2RsVDFWwe84OukT7r02MXFNHWierc5Z/view?usp=drivesdk

* 02/10/2020 - https://meet.google.com/hka-bdtj-rwd
* * Gravação https://drive.google.com/file/d/1kL-iUhn3E-SOQtKSFMIUpNhKyh31aCE2/view?usp=drivesdk

* 08/10/2020 - https://meet.google.com/uvs-heaj-mkf

* 09/10/2020 - https://meet.google.com/cry-xmrz-nge

* 22/10/2020 - https://meet.google.com/uzh-oavq-oqz

* 29/10/2020 - https://meet.google.com/dqm-fmdc-uji

* 30/10/2020 - https://meet.google.com/ctz-tjvc-rwe

* 05/11/2020 - https://meet.google.com/pir-desk-gkt

* 12/11/2020 - https://meet.google.com/pig-hxji-jvf

* 13/11/2020 - https://meet.google.com/ejd-mphr-uhy

* 19/11/2020 - https://meet.google.com/adi-dpok-deb

* 20/11/2020 - https://meet.google.com/pky-xozb-tnj

* 26/11/2020 - https://meet.google.com/zoa-tuai-car

* 27/11/2020 - https://meet.google.com/yfd-iaak-mzs

* 03/12/2020 - https://meet.google.com/zto-ksyh-ard

* 04/12/2020 - https://meet.google.com/xck-jzqr-agj

* 10/12/2020 - https://meet.google.com/vsy-urse-xqj

* 17/12/2020 - https://meet.google.com/iqb-rikx-ate

### Referências

##### Básica


##### Complementar

## IDE

https://www.onlinegdb.com/

## Sumário:

* [01_PlanoEnsino](PlanoEnsinoLTP_II_ECAN_ECTG_prof_RicardoMaia.docx)
* [Por que todos deveriam aprender a programar?](https://youtu.be/mHW1Hsqlp6A)
* [Ranking da Linguagem C](https://www.tiobe.com/tiobe-index/)
* [Estatística do Stackoverflow](https://stackoverflow.com/tags)
